document.querySelector('.menu').addEventListener('click', () => {
  document.querySelectorAll('.objetivo')
    .forEach((item) => {
      item.classList.toggle('mostrar')
    })
})
const iconos = document.querySelectorAll('.seccion-1-iconos i')

let i = 1
setInterval(() => {
  i++
  const icono = document.querySelector('.seccion-1-iconos .cambio')

  icono.classList.remove('cambio')
  if (i > iconos.length) {
    iconos[0].classList.add('cambio')
    i = 1
  } else {
    icono.nextElementSibling.classList.add('cambio')
  }
}, 3000)